package main

import (
	"fmt"
	"sort"
	"strconv"
)

func main() {
	fmt.Println("Hello World")
}

//ReturnInt возвращает единицу
func ReturnInt() int {
	i := 1
	return i
}

//ReturnFloat возвращает флот32
func ReturnFloat() float32 {
	return 1.1
}

//ReturnIntArray вернёт массив
func ReturnIntArray() [3]int {
	return [...]int{1, 3, 4}
}

//ReturnIntSlice вернёт слайс
func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

//IntSliceToString вернёт строку из интов
func IntSliceToString(sl []int) (ret string) {
	for _, value := range sl {
		ret += strconv.Itoa(value)
	}
	return ret
}

//MergeSlices объединит несколько слайсов
func MergeSlices(f []float32, i []int32) []int {
	var ret []int
	for _, val := range f {
		ret = append(ret, int(val))
	}

	for _, val := range i {
		ret = append(ret, int(val))
	}

	return ret
}

//GetMapValuesSortedByKey сортирует мапу по ключу
func GetMapValuesSortedByKey(mm map[int]string) []string {
	var keys []int
	for k := range mm {
		keys = append(keys, k)
	}

	sort.Ints(keys)

	var ret []string
	for _, k := range keys {
		ret = append(ret, mm[k])
	}

	return ret
}
