package main

import (
	"fmt"
	"time"
)

//added markers for understanding :D
func main() {
	println("start")
	myTimer := getTimer()
	println("func")
	f := func() {
		myTimer()
	}
	println("f()")
	f()
}

func getTimer() func() {
	start := time.Now()
	println("gettimer")
	return func() {
		fmt.Printf("Time from start %v", time.Since(start))
	}
}
