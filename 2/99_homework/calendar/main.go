package main

import (
	"time"
)

//struct for Month saving
type MyCalendar struct {
	Month int
}

//get quarter by int of Month inside MyCalendar
func (calendar *MyCalendar) CurrentQuarter() int {
	//idk how to make better with consts
	quartermap := map[int]int{
		1:  1,
		2:  1,
		3:  1,
		4:  2,
		5:  2,
		6:  2,
		7:  3,
		8:  3,
		9:  3,
		10: 4,
		11: 4,
		12: 4,
	}

	//great calculting
	return quartermap[calendar.Month]
}

//just constructor for MyCalendar type
func NewCalendar(t time.Time) *MyCalendar {
	var calendar MyCalendar
	calendar.Month = int(t.Month())
	return &calendar
}

func main() {
	return
}
